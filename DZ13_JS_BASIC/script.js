/* 
 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().

    setTimeout() задає час через який вbконується 1 раз передана йому функція. Тоді як setInterval() задає час, через який виконання переданої йому функції повторюється бескінечно, доки не буде зупинене через clearInterval().

 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

    код (функція) передана в setTimeout() з затримкою 0 спрацює так швидко як тільки буде можливо, але обовязково лише після закінчення виконнаня коду.

 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

    Тому що setInterval() не чекає завершення виконання попереднього виклику функції. Тому якщо непотрібний цикл не зупинити, вони будуть накладатись один на одного. 
*/


// Вирішення завдання:


const start = document.querySelector(".start");
const stop = document.querySelector(".stop");
const restoreSlider = document.querySelector(".continue");

const images = document.querySelectorAll("img");

let countdown = document.querySelector(".countdown");
console.log(countdown);

var currentSlide = 0;

let loop;

// let countDownFunc = function() {
//   let currentTime = parseFloat(countdown.textContent);
//   if (currentTime > 0) {
//     countdown.textContent = currentTime - 1;
//   } else {
//   window.clearInterval(timer);
//   // countdown.textContent = "3";
//   }
// };
// // window.setInterval(countDownFunc, 1000);

function launchSlider() {
  images[currentSlide].classList = "image-to-show";
  currentSlide = (currentSlide + 1) % images.length;
  images[currentSlide].classList = "image-to-show active";
  images[currentSlide].style.animation = "fadeInOut 3s linear";
};



start.addEventListener("click", () => {
  loop = setInterval(launchSlider, 3000);
  stop.classList = "btn stop active";
  restoreSlider.classList = "btn continue active";
});


// АБО:

// window.addEventListener("DOMContentLoaded", () => {
//   loop = setInterval(launchSlider, 3000);
//   stop.classList = "btn stop active";
//   restoreSlider.classList = "btn continue active";
// });

stop.addEventListener("click", () => {
  clearInterval(loop);
  images[currentSlide].style.animation = "";

})

restoreSlider.addEventListener("click", () => {
  loop = setInterval(launchSlider, 3000);
  });




