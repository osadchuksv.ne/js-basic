/*  

1. Які існують типи даних у Javascript?
    Всього є 8 типів даних:
    1. String
    2. Number
    3. BigInt
    4. Boolean
    5. Object
    6. Null
    7. Undefined
    8. Symbol

2. У чому різниця між == і ===?
    Логічний оператор "==" це оператор порівняння зазначенням. В свою чергу "===" - це лоігчний оператор "суворого порівняння" - він порівнює данні не ише ща значенням, а й за типом. До прикладу:
    1 == "1" буде true, але 1 === "1" буде false, так як типи даних, відповідно number та string. І в той же час 1 === 1 - це true. 

3. Що таке оператор?
    Якщо говорити своїми словами, то оператор - це те що визначає спосіб та логіку взаємодії між зміними та значеннями. Існують оператори арифметичні (+, -. /, *, %, ** , ++, --), оператори присвоєння (в чомусь дублюють арифметичні, з додаванням "=": =, +=, -=, *=, /=, %=, **=), оператори порівняння (==, ===, <, <=, >, >=, !=, !==) та логічні оператори (&&, ||, !)
        */

let name = prompt("Ввведіть своє ім'я!");
while (name == null) {
    name = prompt("Будь ласка, введіть своє ім'я!", name);
}

let age = prompt("Введіть свій вік!");
while (age == null || isNaN(age)) {
    age = prompt("Будь ласка, введіть свій вік! Допускаються лише цифри.", age);
}

if (age < 18) {
    alert("You are not allowed to visit this website");
} else if (age >= 18 && age <= 22) {
    let confirmation = confirm("Are you sure you want to continue?");
    if (confirmation == true) {
        alert(`Welcome, ${name}!`);
    } else {
        alert("You are not allowed to visit this website");
    }
} else if (age > 22) {
    alert(`Welcome, ${name}!`);
}





