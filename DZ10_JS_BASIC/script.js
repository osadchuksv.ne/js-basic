/*  

*/
    
// Вирішення завдання:
    
const tabsList = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tabs-content li");

tabsList.forEach((elem) => {
  let tabElem = elem;
  
  elem.addEventListener("click", () => {
    tabsList.forEach((elem) => {
      elem.classList.remove("active");
    });
    elem.classList.add("active");
    
    tabsContent.forEach((elem) => {
      // перший спосіб реалізувати відображення тексту потрібної вкладки (має реалізувати незалежність від зміни назви вкладки):
      
      if(elem.dataset.id != tabElem.getAttribute("id")) {
        elem.classList.remove("visible");
      }
      if(elem.dataset.id === tabElem.getAttribute("id")) {
          elem.classList.add("visible");
        }
      
      /* або другий спосіб:

      if(!tabName.includes(elem.className)) {
        elem.classList.remove("visible");
      }
      if(tabName.includes(elem.className)) {
        elem.classList.add("visible");
       }

      */
    })

  })
});
