/*  

 1. Опишіть своїми словами як працює метод forEach.
   Цей метод призначений для перебору елементів масиву - перебирає почергово всі елементи в масиві і застосовує до них задану функцію.
    
 2. Як очистити масив?
   Якщо ми не маємо посилань на масив в інших місцях коду і нам не потрібно зберегти його першопочатковий вигляд, то можемо очистити масив за допомогою arr = [];
   Також варіанти arr.length = 0; arr.splice(0, arr.length); і методом pop() у циклі while:
   while(arr.length > 0) {
      arr.pop();
   }
 3. Як можна перевірити, що та чи інша змінна є масивом?
    Припустимо, у нас є змінна someTypeData. Ми можемо перевірити чи є вона масивом запустивши Array.isArray(someTypeData);
*/

    
// Вирішення завдання:
    
function filterBy(arr, dataType ) {
   return arr.filter(element => typeof element !== dataType);
}

let arr = ['hello', 'world', 23, '23', null];
let arrFiltered = filterBy(arr, "string");
console.log(arrFiltered);
