/*  

 1. Опишіть своїми словами що таке Document Object Model (DOM).
   У JS усе є об'єктом. По суті, це і є основна ідея Document Object Model (DOM). Увесь HTML представлений глобальний об'єктом document, від якого розгалуженням йде уся структура HTML сторінки. Це називають DOM tree.
    
 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
    innerHTML надає нам доступ до всієї структури елемента окрім самих відкриваючого та закриваючого тегів, тобто усі внутрішні теги елемента з їх атрибутами, властивостями, класами, id та контентом тегів. innerText? надає нам доступ лише до текстового контенту елементу.

 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
    Є два способи.
    а) document.getElementBy... і далі Id()/Tag()/Name()/Class() і т.д.
    б) document.querySelector() / document.querySelectorAll(). Цей спосіб об'єктивно зручніший, так яке дозволяє звертатись до будь-яких елементів просто вказуючи їх у "" так, якби ми звертались до них в CSS + не лише до елементів, а й до їх атрибутів, властивостей тощо. Тобто він лаконічніший та універсальніший. 
*/

    
// Вирішення завдання:
    
document.querySelectorAll("p").forEach(e => {
   e.style.backgroundColor = `#ff0000`;
})
console.log(document.querySelector("#optionsList"));
console.log(document.querySelector("#optionsList").parentElement);
document.querySelector("#optionsList").childNodes.forEach(e => {
   console.log(e);
});
// const opListChildNodes = Array.from(document.querySelector("#optionsList").children);
// opListChildNodes.forEach(e => {
//    console.log(e);
// })

let testP = document.querySelector("#testParagraph").textContent = `This is a paragraph`;
Array.from(document.querySelector(".main-header").children).forEach(e => {
   console.log(e);
   e.classList.add("nav-item");
})
// Array.from(document.querySelector(".main-header").children).forEach(e => {
//    e.classList.add("nav-item");
// })

Array.from(document.querySelectorAll(".section-title")).forEach(e => {
   e.classList.remove("section-title");
})