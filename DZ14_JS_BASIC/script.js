// Вирішення завдання:

// пошук в HTML та присвоєння змінним кнопки зміни теми, елемента body:
const changeThemeBtn = document.querySelector(".btn-change-theme");
const body = document.querySelector("body");
// оголошення змінної для ключа кольрової теми в local storage:
let colorTheme;

// оголошення функції для запису значення "світла тема" чи "темна тема" в local storage:
function storageWrite() {
  if(document.body.className === "dark-theme") {
    localStorage.setItem("colorTheme", "dark-theme");
  } else {
    localStorage.setItem("colorTheme", "light-theme");
  }
};

// оголошення функції для зчитування значення "світла тема" чи "темна тема" з local storage
// та залежно від цього присвоєння або неприсвоєння body класу для відображення темної теми:   
function storageRead() {
  if(typeof localStorage !== "undefined") {
    if(localStorage.getItem("colorTheme") === "dark-theme") {
      document.body.className = "dark-theme";
    } else {
      document.body.className = "";
    }
  }
};


// зчитування значення теми з local storage при завантаженні сторінки:
document.addEventListener("DOMContentLoaded", storageRead);

// реалізація зміни кольорової теми по кліку на кнопці:
changeThemeBtn.addEventListener("click", function toggleClass() {
  document.body.classList.toggle("dark-theme");
  storageWrite();
});

// запис значення теми у local storage перед закриттям/оновленням/перезавантаженням сторінки:
document.addEventListener("unload", storageWrite);








